# textpush - process multiple input text files, make multiple outputs
# Based on <https://github.com/afseo/cmits>.
# Copyright (C) 2015 Jared Jennings, jjennings@fastmail.fm.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from setuptools import setup

setup(
        name='textpush',
        description='Process multiple input text files, make multiple outputs',
        long_description="""\
``textpush`` applies Beazley's push generator ideas
<http://www.dabeaz.com/coroutines> to process input from some files,
tag it, act on its content and tags, and make output go into some
other files. It is born from ``shaney``'s generator code
(<https://github.com/jaredjennings/shaney>).
""",
        version='1.13',
        author='Jared Jennings',
        author_email='jjennings@fastmail.fm',
        license='GPLv3',
        platforms='OS-independent',
        packages=['textpush', 'textpush.test'],
        )

