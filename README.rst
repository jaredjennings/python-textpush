``textpush`` applies Beazley's push generator ideas
<http://www.dabeaz.com/coroutines> to process input from some files,
tag it, act on its content and tags, and make output go into some
other files. It is born from ``shaney``'s generator code
(<https://github.com/jaredjennings/shaney>).
